#include "Main.h"

	vector <Player> players;
	vector <Planet> planets;
	vector <sf::Sprite> dataBkgrnds;
	vector <sf::Text> plyrText;
	vector <sf::RectangleShape> grid;

	bool planetAvailabilty[12] = {true,true,true,true,true,true,true,true,true,true,true,true};
	
	// #########################################################################################################
	// TODO - Find todo in galaxyCreationInput() and drawGalaxyCreationMenu(). 
	// Also planets in the Media/Planets folder majority need to be edited and given unique images
	// Comments - if u see areas that are hard to read or understand - put comments about it.
	// #########################################################################################################

//Game Loop
int main(){ 
	
	srand(time(0));
	startUp();
	
	while(window.isOpen()){
		sf::Event event;
		while(window.pollEvent(event)){
			 if (event.type == sf::Event::Closed)
                window.close();
		}
		if(event.type == sf::Event::MouseButtonPressed){
			mouseX = sf::Mouse::getPosition().x;
			mouseY = sf::Mouse::getPosition().y;
			mouseClick = true;
		}
			animateBackground();
			switch(gameState){
			case GAME_START:
				drawStartMenu();
				startMenuInput();
				break;
			case GAME_BEGIN:
				switch(gameStateBeginSubstate){
				case FACTION_SELECT:
					drawFactionSelectMenu();
					factionSelectInput();
					break;
				case GALAXY_CREATION:
					drawGalaxyCreationMenu();
					galaxyCreationInput();
					break;
				}
				break;
			case GAME_RUN:
				
				break;
			case GAME_END:
				window.close();
				break;
			}
		 
		 window.display();

		 mouseClick = false; //Always keep at end of main loop
	}
	return 0;
}

void startUp(){
	playerCount = 2; //default
	currentPlayer = 0;

	window.setFramerateLimit(30);

	createDefaultPlayers();
	createDefaultPlanets();
	assignPlanets();
	setupMedia();

	gameState = GAME_START;
}

//If a mouse click event occurs, checks which sprite was clicked if any, for the StartMenu
void startMenuInput(){
	if(mouseClick){
		if(buttonPressed(buttonStrtSpr)){
			window.clear();
			gameState = GAME_BEGIN;
			gameStateBeginSubstate = FACTION_SELECT;
		}else if(buttonPressed(buttonQuitSpr))
			window.close();
	}
}

//If a mouse click event occurs, checks which sprite was clicked if any, for the factionSelectMenu
void factionSelectInput(){
	bool isUpdate = false;

	if(mouseClick){
		 if (buttonPressed(howManyBkgrnd)){
			if(buttonPressed(twoPlayers)){
				playerCount = 2;
				resetPlayerData();
				isUpdate = true;
			} else if(buttonPressed(threePlayers)){
				playerCount = 3;
				resetPlayerData();
				isUpdate = true;
			}else if(buttonPressed(fourPlayers)){
				playerCount = 4;
				resetPlayerData();
				isUpdate = true;
			}else if(buttonPressed(fivePlayers)){
				playerCount = 5;
				resetPlayerData();
				isUpdate = true;
			}else if(buttonPressed(sixPlayers)){
				playerCount = 6;
				resetPlayerData();
				isUpdate = true;
			} 
		 }
		 if(currentPlayer < playerCount){
			 if(buttonPressed(aldarisFaction) && !aldarisSelected){
				 players[currentPlayer].setFaction("Aldaris");
				 plyrText[currentPlayer].setString(plyrText[currentPlayer].getString() + players[currentPlayer].getFaction());
				currentPlayer ++;
				aldarisSelected = true;

			 }else if(buttonPressed(jimFaction) && !jimSelected){
				players[currentPlayer].setFaction("Jim Raynor");
				 plyrText[currentPlayer].setString(plyrText[currentPlayer].getString() + players[currentPlayer].getFaction());
				currentPlayer ++;
				jimSelected = true;

			 }else if(buttonPressed(mengskFaction) && !mengskSelected){
				players[currentPlayer].setFaction("Arcturus Mengsk");
				plyrText[currentPlayer].setString(plyrText[currentPlayer].getString() + players[currentPlayer].getFaction());
				currentPlayer ++;
				mengskSelected = true;

			}else if(buttonPressed(queenFaction) && !queenSelected){
				players[currentPlayer].setFaction("Queen Of Blades");
				plyrText[currentPlayer].setString(plyrText[currentPlayer].getString() + players[currentPlayer].getFaction());
				currentPlayer ++;
				queenSelected = true;

			 }else if(buttonPressed(tassadarFaction) && !tassadarSelected){
				players[currentPlayer].setFaction("Tassadar");
				plyrText[currentPlayer].setString(plyrText[currentPlayer].getString() + players[currentPlayer].getFaction());
				currentPlayer ++;
				tassadarSelected = true;

			}else if(buttonPressed(overmindFaction) && !overmindSelected){
				players[currentPlayer].setFaction("The Overmind");
				plyrText[currentPlayer].setString(plyrText[currentPlayer].getString() + players[currentPlayer].getFaction());
				currentPlayer ++;
				overmindSelected = true;
			}				
		 } else
			 factionSelectDone = true;
	}

	if(mouseClick && factionSelectDone){
		if(buttonPressed(nextBtn)){
			window.clear();
			gameStateBeginSubstate = GALAXY_CREATION;
		}
	}

	//The following shows rectangles with "Player 'i' : " on them, i is based on what number is selected above
	if(isUpdate = true){
		for(int i = 2; i < playerCount; i++){
			window.draw(dataBkgrnds.at(i));
			window.draw(plyrText.at(i));
		}
	}
}

//if a mouse click event occurs, check which sprite was clicked if any, for the galaxyCreationMenu
void galaxyCreationInput(){
	// #########################################################################################################
	// TODO - Run through the list of current players getting them to place 1 planet at a time (placement needs to be beside another planet currently placed)
	// Click to select and Click again to place, dont bother wasting time on drag and drop. 
	// Keep going till all planets(2xplayerCount) are placed on the galaxy in valid positions.
	// #########################################################################################################
}

//Draws all the items required for the StartMenu
void drawStartMenu(){
	window.draw(logoSpr);
	window.draw(buttonStrtSpr);
	window.draw(buttonQuitSpr);
}

//Draws all the items required for FactionSelect
void drawFactionSelectMenu(){
	window.draw(howManyBkgrnd);				window.draw(factionBkgrnd);

	window.draw(twoPlayers);				window.draw(threePlayers);			window.draw(fourPlayers);
	window.draw(fivePlayers);				window.draw(sixPlayers);
				
	window.draw(howManyPlayers);			window.draw(selectFaction);	    //Text
	window.draw(dataBkgrnds.at(0));			window.draw(dataBkgrnds.at(1)); //Always will have 2 players, so always have these drawn.
	window.draw(plyrText.at(0));			window.draw(plyrText.at(1));    //Always will have 2 " " " " " .

	if(factionSelectDone)
		window.draw(nextBtn);	

	if(aldarisSelected== false){
        window.draw(aldarisFaction);
    }
    if(jimSelected== false){
        window.draw(jimFaction);
    }
    if(mengskSelected== false){
        window.draw(mengskFaction);
    }
    if(queenSelected== false){
        window.draw(queenFaction);
    }
    if(tassadarSelected== false){
        window.draw(tassadarFaction);
    }
    if(overmindSelected== false){
        window.draw(overmindFaction);
    }
}

//Draws all the items required for GalaxyCreation
void drawGalaxyCreationMenu(){
	//Outline for the region the map will be in.
	gridOutline.setSize(sf::Vector2f(400,500));			
	gridOutline.setPosition(200,50);
	gridOutline.setFillColor(sf::Color::Transparent);	
	gridOutline.setOutlineThickness(3);
	window.draw(gridOutline);
	
	for(int i = 0; i < grid.size(); i++){
		window.draw(grid.at(i));
	}
	
	window.draw(galaxyMapHeader);//Text saying "Galaxy Under Construction"

	window.draw(player1PlanetBkgrnd);		window.draw(player2PlanetBkgrnd);//The transparent bkgrnds
	window.draw(plyr1);						window.draw(plyr2);//The text saying "Player #:"
	window.draw(plyr1Planets);				window.draw(plyr2Planets);//Text displaying planets owned by players
	
	// ##########################################################################################################
	//TODO - Draw Planet sprites stored in planet object stored in the player objects vector of planets they own.
	// Need to assign a position for each sprite still
	// ##########################################################################################################
	
	if(playerCount == 6){
		window.draw(player3PlanetBkgrnd);	window.draw(player4PlanetBkgrnd);	
		window.draw(player5PlanetBkgrnd);	window.draw(player6PlanetBkgrnd);

		window.draw(plyr3);					window.draw(plyr4);	
		window.draw(plyr5);					window.draw(plyr6);

		window.draw(plyr3Planets);			window.draw(plyr4Planets);	
		window.draw(plyr5Planets);			window.draw(plyr6Planets);
	} else if(playerCount == 5){
		window.draw(player3PlanetBkgrnd);	window.draw(player4PlanetBkgrnd);
		window.draw(player5PlanetBkgrnd);

		window.draw(plyr3);					window.draw(plyr4);
		window.draw(plyr5);	

		window.draw(plyr3Planets);			window.draw(plyr4Planets);
		window.draw(plyr5Planets);	
	} else if(playerCount == 4){
		window.draw(player3PlanetBkgrnd);	window.draw(player4PlanetBkgrnd);

		window.draw(plyr3);					window.draw(plyr4);

		window.draw(plyr3Planets);			window.draw(plyr4Planets);
	} else if(playerCount == 3){
		window.draw(player3PlanetBkgrnd);

		window.draw(plyr3);	

		window.draw(plyr3Planets);
	}
	
}

//Connects all the image files(in Media folder) to a sf::Sprite and sets up any texts required.
void setupMedia(){
	//Start Menu 
	try{
		logoTxtr.loadFromFile("Media/GAME_START/StarcraftLogo.png");
		backgroundTxtr.loadFromFile("Media/Stars.jpg");
		buttonStrtTxtr.loadFromFile("Media/GAME_START/Start.png");
		buttonQuitTxtr.loadFromFile("Media/GAME_START/Quit.png");
	} catch(int e){
		cout << "IO Exception, Exception Nr. " << e << endl;
	}
	logoSpr.setTexture(logoTxtr);
	logoSpr.setPosition(100,50);

	backgroundSpr1.setTexture(backgroundTxtr);
	backgroundSpr2.setTexture(backgroundTxtr);

	buttonStrtSpr.setTexture(buttonStrtTxtr);
	buttonStrtSpr.setPosition(260,400);

	buttonQuitSpr.setTexture(buttonQuitTxtr);
	buttonQuitSpr.setPosition(260,460);
	//End Start Menu 


	//Faction Select 
	try{
		//Player Count Buttons
		twoTxtr.loadFromFile("Media/GAME_BEGIN/Numbers/2.png");
		threeTxtr.loadFromFile("Media/GAME_BEGIN/Numbers/3.png");
		fourTxtr.loadFromFile("Media/GAME_BEGIN/Numbers/4.png");
		fiveTxtr.loadFromFile("Media/GAME_BEGIN/Numbers/5.png");
		sixTxtr.loadFromFile("Media/GAME_BEGIN/Numbers/6.png");

		//Semi-Transparent Rectangles
		howManyBkgrndTxtr.loadFromFile("Media/GAME_BEGIN/transBkgrd_1.png");
		factionBkgrndTxtr.loadFromFile("Media/GAME_BEGIN/transBkgrd_2.png");
		playerDataBkgrndTxtr.loadFromFile("Media/GAME_BEGIN/transBkgrd_4.png");

		//Factions
		aldarisTxtr.loadFromFile("Media/GAME_BEGIN/Factions/Aldaris.png");
		jimTxtr.loadFromFile("Media/GAME_BEGIN/Factions/JimRaynor.png");
		mengskTxtr.loadFromFile("Media/GAME_BEGIN/Factions/Mengsk.png");
		queenTxtr.loadFromFile("Media/GAME_BEGIN/Factions/QueenOfBlades.png");
		tassadarTxtr.loadFromFile("Media/GAME_BEGIN/Factions/Tassadar.png");
		overmindTxtr.loadFromFile("Media/GAME_BEGIN/Factions/TheOvermind.png");

		nextTxtr.loadFromFile("Media/GAME_BEGIN/next.png");
		
	} catch(int e){
		cout << "IO Exception, Exception Nr. " << e << endl;
	}
	
	twoPlayers.setTexture(twoTxtr);			twoPlayers.setPosition(70,90);
	threePlayers.setTexture(threeTxtr);		threePlayers.setPosition(115,90);
	fourPlayers.setTexture(fourTxtr);		fourPlayers.setPosition(160,90);
	fivePlayers.setTexture(fiveTxtr);		fivePlayers.setPosition(205,90);
	sixPlayers.setTexture(sixTxtr);			sixPlayers.setPosition(250,90);

	howManyBkgrnd.setTexture(howManyBkgrndTxtr);
	howManyBkgrnd.setPosition(50,50);

	factionBkgrnd.setTexture(factionBkgrndTxtr);
	factionBkgrnd.setPosition(500,190);

	playerDataBkgrnd.setTexture(playerDataBkgrndTxtr);

	aldarisFaction.setTexture(aldarisTxtr);		aldarisFaction.setPosition(515,225);
	jimFaction.setTexture(jimTxtr);				jimFaction.setPosition(515,275);
	mengskFaction.setTexture(mengskTxtr);		mengskFaction.setPosition(515,325);
	queenFaction.setTexture(queenTxtr);			queenFaction.setPosition(515,375);
	tassadarFaction.setTexture(tassadarTxtr);	tassadarFaction.setPosition(515,425);
	overmindFaction.setTexture(overmindTxtr);	overmindFaction.setPosition(515,475);

	nextBtn.setTexture(nextTxtr);				nextBtn.setPosition(515,540);

	int x = 50, y = 200;
	for(int i = 0; i < 6; i++){
		sf::Sprite s = playerDataBkgrnd;
		s.setPosition(x,y);
		dataBkgrnds.push_back(s);
		y+=50;
	}
	//End Faction Select


	//Galaxy Creation
	createGrid();

	try{
		playerPlanetBkgrndTxtr.loadFromFile("Media/GAME_BEGIN/transBkgrd_3.png");
	} catch(int e){
		cout << "IO Exception, Exception Nr. " << e << endl;
	}
	player1PlanetBkgrnd.setTexture(playerPlanetBkgrndTxtr);		player1PlanetBkgrnd.setPosition(10,20);
	player2PlanetBkgrnd.setTexture(playerPlanetBkgrndTxtr);		player2PlanetBkgrnd.setPosition(625,20);
	player3PlanetBkgrnd.setTexture(playerPlanetBkgrndTxtr);		player3PlanetBkgrnd.setPosition(10,210);
	player4PlanetBkgrnd.setTexture(playerPlanetBkgrndTxtr);		player4PlanetBkgrnd.setPosition(625,210);
	player5PlanetBkgrnd.setTexture(playerPlanetBkgrndTxtr);		player5PlanetBkgrnd.setPosition(10,400);
	player6PlanetBkgrnd.setTexture(playerPlanetBkgrndTxtr);		player6PlanetBkgrnd.setPosition(625,400);
	//End Galaxy Creation
	

	//Text
	try{
		font.loadFromFile("Media/starcraft.ttf");
	} catch(int e){
		cout << "IO Exception, Exception Nr. " << e << endl;
	} 
		
	howManyPlayers.setFont(font);
	howManyPlayers.setCharacterSize(18);
	howManyPlayers.setString("How Many Players?:");
	howManyPlayers.setPosition(60,60);

	selectFaction.setFont(font);
	selectFaction.setCharacterSize(18);
	selectFaction.setString("Choose Your Faction:");
	selectFaction.setPosition(505,195);

	galaxyMapHeader.setFont(font);
	galaxyMapHeader.setCharacterSize(18);
	galaxyMapHeader.setString("Galaxy Under Construction:");
	galaxyMapHeader.setPosition(200,10);

		//Text saying "Player #:" - Galaxy Creation
	plyr1.setFont(font);			plyr2.setFont(font);
	plyr3.setFont(font);			plyr4.setFont(font);
	plyr5.setFont(font);			plyr6.setFont(font);

	plyr1.setCharacterSize(18);		plyr2.setCharacterSize(18);
	plyr3.setCharacterSize(18);		plyr4.setCharacterSize(18);
	plyr5.setCharacterSize(18);		plyr6.setCharacterSize(18);

	plyr1.setString("Player 1:");	plyr2.setString("Player 2:");
	plyr3.setString("Player 3:");	plyr4.setString("Player 4:");	
	plyr5.setString("Player 5:");	plyr6.setString("Player 6:");

	plyr1.setPosition(player1PlanetBkgrnd.getPosition().x+5,player1PlanetBkgrnd.getPosition().y+5);
	plyr2.setPosition(player2PlanetBkgrnd.getPosition().x+5,player2PlanetBkgrnd.getPosition().y+5);
	plyr3.setPosition(player3PlanetBkgrnd.getPosition().x+5,player3PlanetBkgrnd.getPosition().y+5);		
	plyr4.setPosition(player4PlanetBkgrnd.getPosition().x+5,player4PlanetBkgrnd.getPosition().y+5);
	plyr5.setPosition(player5PlanetBkgrnd.getPosition().x+5,player5PlanetBkgrnd.getPosition().y+5);
	plyr6.setPosition(player6PlanetBkgrnd.getPosition().x+5,player6PlanetBkgrnd.getPosition().y+5);
		//End text saying "Player #:"

		//Planets name text - Galaxy Creation
	plyr1Planets.setFont(font);		plyr1Planets.setCharacterSize(18);		plyr1Planets.setPosition(plyr1.getPosition().x,plyr1.getPosition().y+65);
	plyr2Planets.setFont(font);		plyr2Planets.setCharacterSize(18);		plyr2Planets.setPosition(plyr2.getPosition().x,plyr2.getPosition().y+65);
	plyr3Planets.setFont(font);		plyr3Planets.setCharacterSize(18);		plyr3Planets.setPosition(plyr3.getPosition().x,plyr3.getPosition().y+65);
	plyr4Planets.setFont(font);		plyr4Planets.setCharacterSize(18);		plyr4Planets.setPosition(plyr4.getPosition().x,plyr4.getPosition().y+65);
	plyr5Planets.setFont(font);		plyr5Planets.setCharacterSize(18);		plyr5Planets.setPosition(plyr5.getPosition().x,plyr5.getPosition().y+65);
	plyr6Planets.setFont(font);		plyr6Planets.setCharacterSize(18);		plyr6Planets.setPosition(plyr6.getPosition().x,plyr6.getPosition().y+65);

	plyr1Planets.setString(players[0].getPlanet(0).getName() + "\n \n \n \n" + players[0].getPlanet(1).getName());
	plyr2Planets.setString(players[1].getPlanet(0).getName() + "\n \n \n \n" + players[1].getPlanet(1).getName());
	plyr3Planets.setString(players[2].getPlanet(0).getName() + "\n \n \n \n" + players[2].getPlanet(1).getName());
	plyr4Planets.setString(players[3].getPlanet(0).getName() + "\n \n \n \n" + players[3].getPlanet(1).getName());
	plyr5Planets.setString(players[4].getPlanet(0).getName() + "\n \n \n \n" + players[4].getPlanet(1).getName());
	plyr6Planets.setString(players[5].getPlanet(0).getName() + "\n \n \n \n" + players[5].getPlanet(1).getName());
		//End Planets name text

	int a = 55, b = 210;
	for(int i = 0; i < 6;  i++){
		sf::Text t;
		stringstream ss;
		ss<< i+1;
		
		t.setFont(font);
		t.setCharacterSize(18);
		if(i != 0)
			t.setString("Player " + ss.str() + " : ");
		else
			t.setString("Player " + ss.str() + "   : "); //Due to OCD...as the number 1 is thinner than the rest thus causing the factions when added to be placed 1 character sooner than the rest.
		t.setPosition(a,b);

		plyrText.push_back(t);
		b+=50;
	}
	//End Text
}

//Does the simple stars moving in the background animation
void animateBackground(){
	if(backgroundXPos >= 800){
		backgroundXPos = 0;
	}
	backgroundXPos+= 0.5;

	int bkgrdSpr1Pos = backgroundXPos;
	int bkgrdSpr2Pos = -800 + backgroundXPos;

	backgroundSpr1.setPosition(bkgrdSpr1Pos,0);
	backgroundSpr2.setPosition(bkgrdSpr2Pos,0);
	
	window.draw(backgroundSpr1);
	window.draw(backgroundSpr2);
}

//Checks if the sprite passed into this method has been clicked, based on whether the mouse coords are within the sprite
bool buttonPressed(sf::Sprite s){
	float sHeight = s.getGlobalBounds().height;
    float sWidth =  s.getGlobalBounds().width;
    if(sf::Mouse::getPosition(window).x >= s.getPosition().x && sf::Mouse::getPosition(window).x <= s.getPosition().x+sWidth
        && sf::Mouse::getPosition(window).y >= s.getPosition().y && sf::Mouse::getPosition(window).y <= s.getPosition().y+sHeight){
		return true;
	} else
		return false;
}

//Creates 6 players to be used for the game (6 is maxPlayers a game can have)
void createDefaultPlayers(){
	for(int i = 0; i < 6; i++){
		Player p(i);
		players.push_back(p);
	}
}

//Creates 12 planets to be used for the game (12 = maxPlayers(6) x 2)
void createDefaultPlanets(){
	for(int i = 0; i < 12; i++){
		Planet p(planetNames[i]);
		string path = "Media/Planets/" + p.getName() + ".png";
		p.setSprite(path);
		planets.push_back(p);
	}
}

//Empties the player faction data and defaults the text displayed on screen during faction select
void resetPlayerData(){
	aldarisSelected = false;jimSelected= false; mengskSelected= false;  
    queenSelected = false; tassadarSelected= false; overmindSelected= false;
	factionSelectDone = false;
	currentPlayer = 0;

	for(int i = 0; i < players.size(); i++){
		players[i].setFaction("");
	}

	for(int i = 0; i < plyrText.size(); i++){
		stringstream ss;
        ss<< i+1;
		if(i != 0)
			plyrText[i].setString("Player " + ss.str() + " : ");
		else
			plyrText[i].setString("Player " + ss.str() + "   : ");
	}
}

/**Assigns 2 planets to each player at random
 * Does not take into account how many players involved
 * as it does not matter, less players means less planets on
 * the map and so the extra ones assigned to players who do not
 * exist are not looked for.
 */
void assignPlanets(){
	bool vpc = false; //valid planet choice
	int x = 0, y =0;
	for(int i = 0; i < players.size(); i++){
		
			x = 0;
		
			while(x < 2){
				vpc = false;
				do{
					y = rand() %12;
				
					if(testPlanetAvailabilty(y)){
						planetAvailabilty[y] = false;
						players[i].addPlanets(x,planets[y]);
						vpc = true;
						
					}	
				}while(!vpc);
				x++;
		}
	}
}

//Checks if a planet has already been assigned
bool testPlanetAvailabilty(int i){
	if(planetAvailabilty[i])
		return true;
	else
		return false;
}

//Draws the grid for the galaxyMap
void createGrid(){
	for(int i = 200; i<600; i+=50){
		for(int j = 50; j<550; j+=50){
			sf::RectangleShape gridSquare;
			gridSquare.setSize(sf::Vector2f(50,50));
			gridSquare.setFillColor(sf::Color::Transparent);
			gridSquare.setOutlineThickness(0.5);
			gridSquare.setPosition(i,j);

			grid.push_back(gridSquare);
		}
	}
}
