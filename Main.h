#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include <ctime>
#include <vector>
#include <string>

#include "Player.h"

using namespace std;

//GENERAL USE - Variables + Methods

int currentPlayer;
sf::RenderWindow window(sf::VideoMode(800, 600), "Stahcrak");
sf::Font font; //Starcraft Font use for all text 

/**
 * GAME_START =		Covers the opening screen + selecting factions. 
 * GAME_BEGIN =		Covers placing planets onto screen to form a galaxy
 * GAME_RUN =		Covers the main game playthrough (ie. plan phase, action phase)
 * GAME_END =		Exit Game
 */
enum GAMESTATE{GAME_START, GAME_BEGIN, GAME_RUN, GAME_END};
GAMESTATE gameState;

/**
 *	FACTION_SELECT = Covers choosing how many players and picking a faction
 *	GALAXY_CREATION = Covers the construction of a galaxy
 */
enum GAME_BEGIN_SUBSTATES{FACTION_SELECT, GALAXY_CREATION};
GAME_BEGIN_SUBSTATES gameStateBeginSubstate;

/**
 *	PLAN_STAGE =		Covers the placement of orders (ie. build or attack)	
 *	ACTION_STAGE =	Covers the combat scene
 */
enum GAME_RUN_SUBSTATES{PLAN_STAGE, ACTION_STAGE};
GAME_RUN_SUBSTATES gameStateRunSubstate;

int mouseX, mouseY;
bool mouseClick;

void setupMedia();
void assignPlanets();
bool testPlanetAvailabilty(int i);
bool buttonPressed(sf::Sprite);
void animateBackground();
void startUp();


//GAME_START - Variables + Methods
//Start Menu
double backgroundXPos;
sf::Sprite backgroundSpr1, backgroundSpr2;
sf::Texture backgroundTxtr;

sf::Sprite buttonStrtSpr;
sf::Texture buttonStrtTxtr;

sf::Sprite buttonQuitSpr;
sf::Texture buttonQuitTxtr;

sf::Sprite logoSpr;
sf::Texture logoTxtr;

void drawStartMenu();
void startMenuInput();

//GAME_BEGIN - Variables + Methods
//Faction Select
sf::Sprite twoPlayers,threePlayers,fourPlayers,fivePlayers,sixPlayers; // Selection of how many
sf::Texture twoTxtr,threeTxtr,fourTxtr,fiveTxtr,sixTxtr;

sf::Sprite howManyBkgrnd, factionBkgrnd, playerDataBkgrnd;
sf::Texture howManyBkgrndTxtr, factionBkgrndTxtr, playerDataBkgrndTxtr;

sf::Sprite pl1,pl2,pl3,pl4,pl5,pl6; // Players
sf::Texture pl1Txtr,pl2Txtr,pl3Txtr,pl4Txtr,pl5Txtr,pl6Txtr;

sf::Text howManyPlayers, selectFaction;

sf::Sprite aldarisFaction,jimFaction,mengskFaction,queenFaction,tassadarFaction,overmindFaction;
sf::Texture aldarisTxtr,jimTxtr,mengskTxtr,queenTxtr,tassadarTxtr,overmindTxtr;

sf::Sprite nextBtn,beginBtn;
sf::Texture nextTxtr,beginTxtr;

bool aldarisSelected,jimSelected,mengskSelected,queenSelected,tassadarSelected,overmindSelected;
bool factionSelectDone;

int playerCount;

void createDefaultPlayers();
void drawFactionSelectMenu();
void factionSelectInput();
void resetPlayerData();


//GalaxyCreation
const string planetNames[12] = { "Aiur", "Zerus", "Earth", "Char", "Mar Sara", "Dylar IV", "Nidhogg", "Helios", "Korhal", "Tarsonis", "Haven", "Ash'Arak"};

sf::Sprite player1PlanetBkgrnd,player2PlanetBkgrnd,player3PlanetBkgrnd,player4PlanetBkgrnd,player5PlanetBkgrnd,player6PlanetBkgrnd;
sf::Texture playerPlanetBkgrndTxtr;

sf::RectangleShape gridOutline;

sf::Text galaxyMapHeader;
sf::Text plyr1,plyr2,plyr3,plyr4,plyr5,plyr6; //Showing Player1 Player2 etc on the galaxy screen
sf::Text plyr1Planets,plyr2Planets,plyr3Planets,plyr4Planets,plyr5Planets,plyr6Planets;

void createDefaultPlanets();
void galaxyCreationInput();
void drawGalaxyCreationMenu();
void createGrid();