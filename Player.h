#include <string>
#include <iostream>
#include <vector>
#include <sstream>

#include "Planet.h";

using namespace std;

class Player{
private:
	int playerID;
	string faction;
	vector <Planet> playersPlanets;

public:
	
		 Player(int);
	void setPlayerID(int);
	void setFaction(string);
	void addPlanets(int i, Planet);

	int getPlayerID();
	string getFaction();
	Planet getPlanet(int);
	string toString();
};
