#include "Planet.h"

Planet::Planet(string s){
	planetName =  s;
}

string Planet::getName(){
	return planetName;
}

sf::Sprite Planet::getSprite(){
	return planetImg;
}

void Planet::setName(string s){
	planetName = s;
}

void Planet::setSprite(string path){
	try{
		planetTxtr.loadFromFile(path);
	} catch(int e){
		cout << "IO Exception, Exception Nr. " << e << endl;
	}
	planetImg.setTexture(planetTxtr);
}

void Planet::setSpiteLocation(int x, int y){
	planetImg.setPosition(x,y);
}
int Planet::getSpiteLocation(){
	return planetImg.getPosition().x;
}