#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>

using namespace std;

class Planet{
private:
	string planetName;
	sf::Texture planetTxtr;
	sf::Sprite planetImg;

public:
	
		 Planet(string);
	string getName();
	sf::Sprite getSprite();
	void setName(string);
	void setSprite(string);
	void setSpiteLocation(int,int);
	int getSpiteLocation();
};
