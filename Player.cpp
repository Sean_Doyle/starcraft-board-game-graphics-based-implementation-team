#include "Player.h"


Player::Player(int id){
    playerID = id;
    faction = "";
}

void Player::setPlayerID(int i){
    playerID = i;
}
void Player::setFaction(string s){
    faction = s;
}
void Player::addPlanets(int i, Planet p){
    playersPlanets.push_back(p);
}   
int Player::getPlayerID(){
    return playerID;
}
string Player::getFaction(){
    return faction;
}
Planet Player::getPlanet(int i){
    return playersPlanets[i];
}
string Player::toString(){
    string details, ownedPlanets;

    stringstream ss;
    ss << playerID;
    
    details = "Player " + ss.str() + " : " + faction + "\nOwned Planets: ";

    for(int i = 0; i < playersPlanets.size(); i++){
        ownedPlanets += playersPlanets[i].getName() + "; ";
    }
    details += ownedPlanets + "\n";
    cout << playersPlanets.size();
    return details;
}